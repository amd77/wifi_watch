#!/usr/bin/env python3

import datetime
import os
import json
import requests
import socket
import time
import yaml
from lxml import etree


class TimeData:
    def __init__(self, forget):
        self.forget = forget
        self.since = None
        self.count = 0
        self.until = None

    def update(self, dt):
        if not self.since or not self.until or dt.timestamp() - self.until.timestamp() > self.forget:
            self.since = dt
            self.count = 0
        if not self.until or self.until != dt:
            self.count += 1
            self.until = dt

    def sort_key(self):
        return -self.until.timestamp()

    def is_latest(self, now=None):
        if not now:
            return True
        else:
            return now.timestamp()-self.until.timestamp() < self.forget

    def __str__(self):
        return f"seen {self.count} times since {self.since} until {self.until.time()} (forget={self.forget})"


class TimeGroup:
    def __init__(self, extension, now, forget_default, forget_values):
        self.extension = extension
        self.now = now
        self.forget_default = forget_default
        self.values = {}
        self.forget_values = forget_values

    def filename(self):
        return f"log/{self.now.year:04d}/{self.now.month:02d}/{self.now.day:02d}{self.extension}"

    def makedir_if_not_exists(self):
        filename = self.filename()
        dirname = os.path.dirname(filename)
        if not os.path.exists(dirname):
            os.makedirs(dirname)

    def readlines(self):
        filename = self.filename()
        if os.path.exists(filename):
            for line in open(filename, "r").readlines():
                try:
                    dt, *values = line.strip().split(",")
                    yield datetime.datetime.fromtimestamp(float(dt)), values
                except ValueError as e:
                    print("ValueError al leer linea {e}")

    def readfile(self):
        for dt, values in self.readlines():
            self.update(dt, values)

    def trimfile(self):
        self.makedir_if_not_exists()
        filename = self.filename()
        open(filename, "w").write("")

    def update(self, dt, values):
        self.now = dt  # update filename
        for value in values:
            if value not in self.values:
                forget = self.forget_values.get(value, self.forget_default)
                d = TimeData(forget)
                self.values[value] = d
            else:
                d = self.values[value]
            d.update(dt)

    def save(self, dt, values):
        self.now = dt  # update filename
        filename = self.filename()
        self.makedir_if_not_exists()
        line = f"{dt.timestamp()}," + ",".join(sorted(values)) + "\n"
        open(filename, "a").write(line)

    def update_and_save(self, dt, values):
        self.update(dt, values)
        self.save(dt, values)

    def latest(self, now=None):
        values_d = [(value, d) for value, d in self.values.items() if d.is_latest(now)]
        values_d = sorted(values_d, key=lambda vd: vd[1].sort_key())
        return [value for value, dt in values_d]

    def print(self, now=None):
        for value in self.latest(now):
            d = self.values[value]
            print(f"- '{value}' {d}")


def run_nmap_get_macs(cmd):
    scandata = os.popen(cmd).read()
    scandata = scandata.encode('utf-8')
    doc = etree.fromstring(scandata)
    macs = {}
    for status in doc.findall('.//status[@reason="arp-response"]/..'):
        address = status.find("address[@addrtype='mac']")
        mac = address.attrib.get("addr")
        vendor = address.attrib.get("vendor")
        ip = status.find("address[@addrtype='ipv4']")
        if ip is not None:
            ip = ip.attrib.get("addr")
        hostname = status.find("hostname[@type='PTR']")
        if hostname is not None:
            hostname = hostname.attrib.get("name")
        macs[mac] = {'vendor': vendor, 'ip': ip, 'hostname': hostname}
    return macs


class Hosts:
    def __init__(self, configdict):
        self.hostnames = {}
        self.macs = {}
        self.notify = {}
        for hostname, data in configdict.items():
            self.hostnames[hostname] = data['macs']
            for mac in data['macs']:
                self.macs[mac] = hostname
            self.notify[hostname] = data.get('notify', True)

    def mac_to_hostname(self, mac):
        return self.macs.get(mac, f"{mac}")

    def macs_to_hostnames(self, macs):
        out = [self.mac_to_hostname(mac) for mac in macs]
        return sorted(set(out))

    def macdict_to_hostnamedict(self, macdict):
        out = {}
        for mac, data in macdict.items():
            hostname = self.mac_to_hostname(mac)
            if not hostname in out:
                out[hostname] = {}
            for k, v in data.items():
                if k not in out[hostname]:
                    out[hostname][k] = f"{v} "
                else:
                    out[hostname][k] += f"{v} "
        return out


def telegram_send_message(message, config):
    command = "sendMessage"
    url = f"{config['bot_url']}/bot{config['bot_token']}/{command}"
    for chat_id in config['bot_chatids']:
        json_data = {"chat_id": chat_id, "text": message, "parse_mode": 'HTML'}
        try:
            requests.post(url, json=json_data)
        except socket.gaierror as e:
            print(f"Error al enviar mensaje '{message}': {e}")


def maclist_update_last(timestamp, maclist, filename="last.json"):
    filename2 = filename + ".new"
    last = json.load(open(filename))
    for mac in maclist:
        mac = mac.lower()
        if mac and (mac not in last or last[mac] < timestamp):
            last[mac] = int(timestamp)
    try:
        json.dump(last, open(filename2, "w"), indent=1, sort_keys=True)
        os.rename(filename2, filename)
    except Exception as e:
        print(f"Error al escribir {filename}: {e.__class__.__name__} {e}")


if __name__ == "__main__":
    config_filename = "wifi_watch.yaml"
    config = yaml.load(open(config_filename))
    config_changed = os.path.getctime(config_filename)

    dns = Hosts(config["hosts"])

    repeat = config['daemon']['repeat']
    forget = config['daemon']['forget']
    nmap = config['daemon']['nmap']
    forget_hostnames = {host: values['forget']
                        for host, values in config['hosts'].items() if 'forget' in values}
    forget_macs = {mac: values['forget']
                   for host, values in config['hosts'].items() if 'forget' in values
                   for mac in values['macs']}

    # Lee fichero macs (primario)
    now = datetime.datetime.now()
    t_mac = TimeGroup("_mac.csv", now, forget, forget_macs)
    t_mac.readfile()

    # Regenerando fichero hostnames (secundario)
    print("Rebuilding history")
    t_hostname = TimeGroup("_hostname.csv", now, forget, forget_hostnames)
    t_hostname.trimfile()
    for start, maclist in t_mac.readlines():
        hostnamelist = dns.macs_to_hostnames(maclist)
        t_hostname.update_and_save(start, hostnamelist)
    t_hostname.print()

    # Tiempo real
    online_last = set(t_hostname.latest(now))
    while config_changed == os.path.getctime(config_filename):
        now = datetime.datetime.now().replace(microsecond=0)
        print(now)
        macdict = run_nmap_get_macs(nmap)
        print(macdict)
        maclist = list(macdict.keys())
        # Generando fichero macs
        t_mac.update_and_save(now, maclist)
        # Generando fichero hostnames
        hostnamelist = dns.macs_to_hostnames(maclist)
        hostnamedict = dns.macdict_to_hostnamedict(macdict)
        t_hostname.update_and_save(now, hostnamelist)
        t_hostname.print(now)
        online = set(t_hostname.latest(now))
        for hostname in online-online_last:
            print(f"{now} {hostname} ONLINE")
            if dns.notify.get(hostname, config['telegram']['default']):
                when = now.strftime("%H:%M")
                msg = config["telegram"]["message_online"].format(name=hostname, when=when, **hostnamedict[hostname])
                telegram_send_message(msg, config['telegram'])
        for hostname in online_last-online:
            print(f"{now} {hostname} OFFLINE")
            if dns.notify.get(hostname, config['telegram']['default']):
                minutes = forget_hostnames.get(hostname, forget) // 60
                when = (now - datetime.timedelta(seconds=minutes*60)).strftime("%H:%M")
                msg = config["telegram"]["message_offline"].format(name=hostname, minutes=minutes, when=when)
                telegram_send_message(msg, config['telegram'])
        online_last = online
        maclist_update_last(now.timestamp(), maclist)
        # durmiendo para siguiente
        time.sleep(max(now.timestamp()+repeat-datetime.datetime.now().timestamp(), 0))
