#!/usr/bin/env python

import glob
import json

last = {}
for filename in glob.glob("log/*/*/*_mac.csv"):
    print(filename)
    for numlinea, linea in enumerate(open(filename).readlines()):
        try:
            macs = linea.strip().split(",")
            timestamp = int(float(macs.pop(0)))
            for mac in macs:
                mac = mac.lower()
                if mac and (mac not in last or last[mac] < timestamp):
                    last[mac] = timestamp
        except ValueError:
            print(f"fichero {filename} linea {numlinea+1} incorrecta")
print(last)
json.dump(last, open("last.json", "w"), indent=1, sort_keys=True)
