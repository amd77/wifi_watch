# Wifi Watch

Servicio para vigilar las macs que hay en una red local y anunciar las altas y
bajas por telegram.

Las altas se anuncian inmediatamente. Las bajas se anuncian pasados un número
de segundos configurable.

## Características

* Fichero yaml de configuracion que dispone de:
    * Asociaciones mac-nombre para los dispositivos conocidos
    * Permite no notificar ciertos dispositivos que no nos interesan
    * Permite personalizar tiempo de olvido para cada dispositivo
    * El ejecutable comprueba si el fichero cambia, y se detiene (para su reinicio)
* Se guarda una carpeta log, con subcarpetas de año y mes, con la salida del nmap
    * Un fichero con timestamp y macs
    * Otro fichero con timestamp y hostnames
* Envío por telegram
    * Permite definir los mensajes de alta y de baja
    * Permite enviar a múltiples destinatarios


## Instalación

```
apt-get install nmap python3-yaml python3-requests python3-lxml
cd /opt/
git clone https://gitlab.com/amd77/wifi_watch.git
cd wifi_watch
cp wifi_watch.yaml.sample wifi_watch.yaml
vi wifi_watch.yaml  # a tu criterio
./wifi_watch.py
```

## Instalación como supervisor

Para instalarlo como servicio en `supervisor`:

```
apt-get install supervisor
vi /etc/supervisor/conf.d/wifi_watch.conf  # con el contenido que sigue
supervisorctl update
```

```
[program:wifi_watch]
command=/usr/bin/python3 -u wifi_watch.py
directory=/opt/wifi_watch/
user=root
autostart=True
autorestart=True
redirect_stderr=True
```
